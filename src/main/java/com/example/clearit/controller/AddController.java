package com.example.clearit.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.clearit.domain.Ausgabe;
import com.example.clearit.domain.Person;

@RestController("/add")
public class AddController extends ControllerBase {

	@PostMapping(path = "/addPerson", consumes = "application/json", produces = "application/json")
	public Person addPerson(@RequestBody Person person) {
		return pSercive.save(person);
	}
	
	@PostMapping(path = "/addAusgabe", consumes = "application/json", produces = "application/json")
	public Ausgabe addAusgabe(@RequestBody Ausgabe ausgabe) {
		ausgabe.getZahler();
		ausgabe.getBeteiligtePersonen();
		ausgabe.getBetrag();
		return aService.saveAusgabe(ausgabe);
	}

}
