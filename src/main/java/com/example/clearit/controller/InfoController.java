package com.example.clearit.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.clearit.domain.Ausgabe;
import com.example.clearit.domain.Person;
import com.example.clearit.domain.Transaktion;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

@RestController("/info")
public class InfoController extends ControllerBase {


	@GetMapping(path = "/getPersons", produces = "application/json")
	public String getPersons() throws JsonProcessingException {
		List<Person> persons = pSercive.listAll();
		ObjectMapper om = new ObjectMapper();
		String jsonString = om.writeValueAsString(persons);
		return jsonString;
	}

	@GetMapping(path = "/getAusgaben", produces = "application/json")
	public String getAusgaben() throws JsonProcessingException {
		List<Ausgabe> ausgaben = aService.listAll();
		ObjectMapper om = new ObjectMapper();
		JavaTimeModule javaTimeModule = new JavaTimeModule();
		javaTimeModule.addDeserializer(LocalDate.class,
				new LocalDateDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		javaTimeModule.addSerializer(LocalDate.class,
				new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		om.registerModule(javaTimeModule);
		String jsonString = om.writeValueAsString(ausgaben);
		return jsonString;
	}

	@GetMapping(path = "/getTransaktionen", produces = "application/json")
	public String getTransaktionen() throws JsonProcessingException {
		List<Transaktion> transaktionen = tService.listAll();
		ObjectMapper om = new ObjectMapper();
		String jsonString = om.writeValueAsString(transaktionen);
		return jsonString;
	}

	@GetMapping(path = "/berechneTransaktionen", produces = "application/json")
	public String berechneTransaktionen() throws JsonProcessingException {
		calc.guthabenUndSchuldenNeuBerechnen();
		calc.transaktionenNeuBerechnen();
		List<Transaktion> transaktionen = tService.listAll();
		ObjectMapper om = new ObjectMapper();
		String jsonString = om.writeValueAsString(transaktionen);
		return jsonString;
	}


}
