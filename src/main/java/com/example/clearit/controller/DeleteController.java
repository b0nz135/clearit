package com.example.clearit.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.clearit.domain.Ausgabe;
import com.example.clearit.domain.Person;

@RestController("/delete")
public class DeleteController extends ControllerBase {

	@DeleteMapping("/deleteausgabe/{id}")
	public String deleteAusgabe(@PathVariable Integer id) {
		aService.deleteAusgabe(id);
		return "Löschen erfolgreich";
	}
	
	@DeleteMapping("/deleteperson/{id}")
	public String deletePerson(@PathVariable Integer id) {
		List<Ausgabe> ausgabenList = aService.listAll();
		boolean personBeteiligt = false;
		for (Ausgabe ausgabe : ausgabenList) {
			List<Person> beteiligtePersonen = ausgabe.getBeteiligtePersonen();
			for (Person person : beteiligtePersonen) {
				if(person.getPersonId().equals(id)) {
					personBeteiligt = true;
					break;
				}
			}
			if(personBeteiligt) {
				break;
			}
		}
		if (!personBeteiligt) {
			pSercive.deletePersonById(id);
			return "Löschen erfolgreich.";
		} else {
			return "Die Person mit der id=" + id + " kann nicht gelöscht werden. Sie ist an Ausgaben beteiligt.";
		}
	}

	
}
