package com.example.clearit.controller;

import java.time.LocalDate;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.clearit.domain.Ausgabe;
import com.example.clearit.domain.Person;
import com.example.clearit.domain.Transaktion;

@RestController("/init")
public class InitController extends ControllerBase{
	
	@GetMapping("/initPersons")
	public String initPersons() {
		Person tom = new Person();
		tom.setName("Tom");
		Person chrizzle = new Person();
		chrizzle.setName("Chrizzle");
		Person anne = new Person();
		anne.setName("Anne");

		Transaktion t1 = new Transaktion();
		t1.setBetrag(49.75);
		t1.setVon(anne);
		t1.setAn(chrizzle);

		Ausgabe a1 = new Ausgabe();
		a1.setAusgabenTag(LocalDate.now());
		a1.setCreationDate(LocalDate.now());
		a1.setBetrag(30.0);
		a1.setZahler(tom);
		a1.getBeteiligtePersonen().add(anne);
		a1.getBeteiligtePersonen().add(tom);
		a1.getBeteiligtePersonen().add(chrizzle);

		anne.setSchulden(anne.getSchulden() + 10);
		chrizzle.setSchulden(chrizzle.getSchulden() + 10);
		tom.setGuthaben(tom.getGuthaben() + 20);

		tom = pSercive.save(tom);
		anne = pSercive.save(anne);
		chrizzle = pSercive.save(chrizzle);
		aService.saveAusgabe(a1);
		tService.save(t1);

		return "...Personen hinzugefügt, Ausgabe erzeugt und gespeichert.";
	}

}
