package com.example.clearit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Clearit1Application {

	public static void main(String[] args) {
		SpringApplication.run(Clearit1Application.class, args);
	}

}
