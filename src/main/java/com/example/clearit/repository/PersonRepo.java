package com.example.clearit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.clearit.domain.Person;

public interface PersonRepo extends JpaRepository<Person, Integer>{

	List<Person> findByName(String name);

	@Modifying
	@Query("update Person p set p.schulden = 0.0, p.guthaben = 0.0")
	void resetSchuldenAndGuthaben();
	
}
