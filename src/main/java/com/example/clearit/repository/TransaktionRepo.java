package com.example.clearit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.clearit.domain.Transaktion;

public interface TransaktionRepo extends JpaRepository<Transaktion, Integer> {
	
	List<Transaktion> deleteByBezahltAmIsNull();

}
