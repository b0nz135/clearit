package com.example.clearit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.clearit.domain.Ausgabe;

public interface AusgabeRepo extends JpaRepository<Ausgabe, Integer>{

}
