package com.example.clearit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.clearit.domain.Transaktion;
import com.example.clearit.repository.TransaktionRepo;

@Service
@Transactional
public class TransaktionService {
	
	@Autowired
	private TransaktionRepo tRepo;
	
	public List<Transaktion> listAll(){
		return tRepo.findAll();
	}
	
	public Transaktion save(Transaktion t) {
		return tRepo.save(t);
	}
	
	public void deleteUnpaied() {
		tRepo.deleteByBezahltAmIsNull();
	}

}
