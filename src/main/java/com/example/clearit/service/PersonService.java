package com.example.clearit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.clearit.domain.Person;
import com.example.clearit.repository.PersonRepo;

@Service
@Transactional
public class PersonService {

	@Autowired
	PersonRepo personRepo;
	
	public List<Person> listAll(){
		return personRepo.findAll();
	}
	
	public List<Person> findByName(String name) {
		return personRepo.findByName(name);
	}
	
	public void resetSchuldenAndGuthaben() {
		personRepo.resetSchuldenAndGuthaben();
	}
	
	public Optional<Person> findById(Integer id) {
		return personRepo.findById(id);
	}
	
	public Person save(Person p) {
		return personRepo.save(p);
	}
	
	public long countAllPersons() {
		return personRepo.count();
	}
	
	public void deletePersonById(Integer id) {
		personRepo.deleteById(id);
	}
	
	public void deleteAll() {
		personRepo.deleteAll();
	}
}
