package com.example.clearit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.clearit.domain.Ausgabe;
import com.example.clearit.repository.AusgabeRepo;

@Service
@Transactional
public class AusgabenService {
	
	@Autowired
	private AusgabeRepo ausgabenRepo;
	
	public Ausgabe saveAusgabe(Ausgabe a) {
		return ausgabenRepo.save(a);
	}

	public List<Ausgabe> listAll(){
		return ausgabenRepo.findAll();
	}
	
	public void deleteAusgabe(Integer ausgabenId) {
		ausgabenRepo.deleteById(ausgabenId);
	}
}
