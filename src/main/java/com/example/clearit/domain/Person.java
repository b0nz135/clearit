package com.example.clearit.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Person {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer personId;
	private String name = "";
	private Double guthaben = 0.0;
	private Double schulden = 0.0;
	
	@ManyToMany(mappedBy="beteiligtePersonen")
	private List<Ausgabe> beteiligteAusgaben = new ArrayList<>();
	
	public Double getGuthaben() {
		return guthaben;
	}
	public void setGuthaben(Double guthaben) {
		this.guthaben = guthaben;
	}
	public Double getSchulden() {
		return schulden;
	}
	public void setSchulden(Double schulden) {
		this.schulden = schulden;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPersonId() {
		return personId;
	}
	public void setPersonId(Integer personId) {
		this.personId = personId;
	}
}
