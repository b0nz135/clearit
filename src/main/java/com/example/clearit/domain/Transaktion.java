package com.example.clearit.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Transaktion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer transaktionId;
	private Double betrag = 0.0;
	private LocalDate bezahltAm;

	@OneToOne
	private Person von;
	@OneToOne
	private Person an;
	
	
	public Integer getTransaktionId() {
		return transaktionId;
	}
	public void setTransaktionId(Integer transaktionId) {
		this.transaktionId = transaktionId;
	}
	public Double getBetrag() {
		return betrag;
	}
	public void setBetrag(Double betrag) {
		this.betrag = betrag;
	}
	public LocalDate getBezahltAm() {
		return bezahltAm;
	}
	public void setBezahltAm(LocalDate bezahltAm) {
		this.bezahltAm = bezahltAm;
	}
	public Person getVon() {
		return von;
	}
	public void setVon(Person von) {
		this.von = von;
	}
	public Person getAn() {
		return an;
	}
	public void setAn(Person an) {
		this.an = an;
	}
}
