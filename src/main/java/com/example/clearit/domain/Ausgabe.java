package com.example.clearit.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Ausgabe {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer ausgabeId;
	private Double betrag = 0.0;
	private boolean kzIstRueckzahlung;
	private LocalDate creationDate;
	private LocalDate ausgabenTag;
	
	@ManyToOne
	private Person zahler;
	
	@ManyToMany
	private List<Person> beteiligtePersonen = new ArrayList<>();
	
	public Integer getAusgabeId() {
		return ausgabeId;
	}

	public void setAusgabeId(Integer ausgabeId) {
		this.ausgabeId = ausgabeId;
	}

	public Double getBetrag() {
		return betrag;
	}

	public void setBetrag(Double betrag) {
		this.betrag = betrag;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDate getAusgabenTag() {
		return ausgabenTag;
	}

	public void setAusgabenTag(LocalDate ausgabenTag) {
		this.ausgabenTag = ausgabenTag;
	}

	public List<Person> getBeteiligtePersonen() {
		return beteiligtePersonen;
	}

	public void setBeteiligtePersonen(List<Person> beteiligtePersonen) {
		this.beteiligtePersonen = beteiligtePersonen;
	}

	public Person getZahler() {
		return zahler;
	}

	public void setZahler(Person zahler) {
		this.zahler = zahler;
	}

	public boolean isKzIstRueckzahlung() {
		return kzIstRueckzahlung;
	}

	public void setKzIstRueckzahlung(boolean kzIstRueckzahlung) {
		this.kzIstRueckzahlung = kzIstRueckzahlung;
	}

}
