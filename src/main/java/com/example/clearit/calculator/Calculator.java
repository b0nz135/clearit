package com.example.clearit.calculator;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.clearit.domain.Ausgabe;
import com.example.clearit.domain.Person;
import com.example.clearit.domain.Transaktion;
import com.example.clearit.service.AusgabenService;
import com.example.clearit.service.PersonService;
import com.example.clearit.service.TransaktionService;

@Component
@Transactional
public class Calculator {
	
	@Autowired
	private TransaktionService tService;
	@Autowired
	private PersonService pService;
	@Autowired
	private AusgabenService aService;
	
	public void transaktionenNeuBerechnen() {
		List<Person> listAll = pService.listAll();
		tService.deleteUnpaied();
		List<Person> sortedGuthaben = listAll.stream().sorted(Comparator.comparing(Person::getGuthaben).reversed()).collect(Collectors.toList());
		List<Person> sortedSchulden = listAll.stream().sorted(Comparator.comparing(Person::getSchulden).reversed()).collect(Collectors.toList());
		for (Person glaeubiger : sortedGuthaben) {
			if(glaeubiger.getGuthaben().equals(0.0)) {
				return;
			}
			for (Person schuldner : sortedSchulden) {
				Transaktion t = new Transaktion();
				t.setAn(glaeubiger);
				t.setVon(schuldner);
				if(schuldner.getSchulden()>=glaeubiger.getGuthaben()) {
					t.setBetrag(glaeubiger.getGuthaben());
					if(schuldner.getSchulden() > glaeubiger.getGuthaben()) {
						schuldner.setSchulden(schuldner.getSchulden()-glaeubiger.getGuthaben());
					}else {
						schuldner.setSchulden(0.0);
					}
					glaeubiger.setGuthaben(0.0);
					tService.save(t);
					break;
				}else {
					t.setBetrag(schuldner.getSchulden());
					glaeubiger.setGuthaben(glaeubiger.getGuthaben()-schuldner.getSchulden());
					schuldner.setSchulden(0.0);
					tService.save(t);
				}
			}
		}
	}
	
	public void guthabenUndSchuldenNeuBerechnen() {
		pService.resetSchuldenAndGuthaben();
		List<Ausgabe> alleAusgaben = aService.listAll();
		for (Ausgabe ausgabe : alleAusgaben) {
			Person zahler = ausgabe.getZahler();
			List<Person> beteiligtePersonen = ausgabe.getBeteiligtePersonen();
			boolean eigenanteilZahler = false;
			Double neuerAnteil = ausgabe.getBetrag() / beteiligtePersonen.size();
			// Jede beiteiligte Person bekommt die Gesamtschulden erhöht.
			for (Person p : beteiligtePersonen) {
				// Falls der Ausgabenzahler dabei ist, dann werden seine Schulden nicht erhöht
				// da er sonst sich selbst Geld schulden würde.
				if(p.equals(zahler)) {
					eigenanteilZahler = true;
				}else {
					p.setSchulden(p.getSchulden() + neuerAnteil);
				}
			}
			// Wenn der Zahler nicht unter den an der Ausgabe beteiligten Personen ist, 
			// dann bekommt er den kompletten Betrag der Ausgabe als Guthaben gutgeschrieben.
			// Andernfalls wird sein Eigenanteil vom Guthaben abgezogen.
			if(!eigenanteilZahler) {
				neuerAnteil = 0.0;
			}
			zahler.setGuthaben(ausgabe.getBetrag()-neuerAnteil);
		}
		
	}

}
