package com.example.clearit.apidocu;

import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@WebAppConfiguration
@Ignore
public class ApiDocumentationJUnit4Test extends AbstractJUnit4SpringContextTests {

	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");
	
	@Autowired
	private WebApplicationContext context;
	
	private RestDocumentationResultHandler documentationHandler;
	
	private MockMvc mockMvc;
	 
	@Before
	public void setUp(){
		this.documentationHandler = document("{method-name}",
				preprocessRequest(prettyPrint()),
				preprocessResponse(prettyPrint()));
	    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
	      .apply(documentationConfiguration(this.restDocumentation))
	      .alwaysDo(this.documentationHandler)
	      .build();
	}
	
	@Test
	public void getPersonsExample() throws Exception {
		this.mockMvc.perform(get("/getPersons")).andExpect(status().isOk())
			.andDo(document("getPersons", links(linkWithRel("/getPersons").description("Listet alle Personen.")),
					responseFields(subsectionWithPath("_links")
					          .description("Links to other resources")),
					        responseHeaders(headerWithName("Content-Type")
					          .description("The Content-Type of the payload"))));
	}

}
