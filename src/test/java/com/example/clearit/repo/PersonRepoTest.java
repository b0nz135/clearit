package com.example.clearit.repo;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.clearit.domain.Person;
import com.example.clearit.service.PersonService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonRepoTest {
	
	@Autowired
	PersonService personService;

	@Before
	public void setUp() throws Exception {
		personService.deleteAll();
		Person p = new Person();
		p.setName("Franz");
		p.setGuthaben(0.46);
		p.setSchulden(780.0);
		Person p2 = new Person();
		p2.setName("Conrad");
		p2.setGuthaben(43.0);
		p2.setSchulden(3.0);
		personService.save(p);
		personService.save(p2);
	}

	@Test
	public void countAllPersonsTest() {
		long anzahlPersonen = personService.countAllPersons();
		Assert.assertEquals(2, anzahlPersonen);
	}
	
	@Test
	public void loadPersonByNameTest() {
		List<Person> resultList = personService.findByName("Franz");
		Assert.assertEquals(1, resultList.size());
		Assert.assertEquals("Franz", resultList.get(0).getName());
		resultList = personService.findByName("Michael");
		Assert.assertTrue(resultList.isEmpty());
	}

	@Test
	public void resetSchuldenAndGuthabenTest() {
		personService.resetSchuldenAndGuthaben();
		List<Person> resultList = personService.findByName("Franz");
		Assert.assertEquals(1, resultList.size());
		Assert.assertEquals(Double.valueOf(0.0), resultList.get(0).getGuthaben());
		resultList = personService.findByName("Conrad");
		Assert.assertEquals(1, resultList.size());
		Assert.assertEquals(Double.valueOf(0.0), resultList.get(0).getGuthaben());
		Assert.assertEquals(Double.valueOf(0.0), resultList.get(0).getSchulden());
	}

	
	
}
